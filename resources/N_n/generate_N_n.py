import sys

if len(sys.argv) != 2:
    print("Expected a single argument")
    exit(1)

n = int(sys.argv[1])
filename = "N_{}.txt".format(sys.argv[1])

print("Generating {}".format(filename))
f = open(filename, "w")

f.write("{}\n\n".format(n))

for i in range(n):
    for j in range(n):
        f.write("0")

        if j != n - 1:
            f.write(" ")
        else:
            f.write("\n")

f.close()
