import sys

if len(sys.argv) != 2:
    print("Expected a single argument")
    exit(1)

s = int(sys.argv[1])
filename = "K_2_linked_{}.txt".format(sys.argv[1])

print("Generating {}".format(filename))
f = open(filename, "w")

n = s + 2
f.write("{}\n\n".format(n))

for i in range(n):
    for j in range(n):
        if i == j:
            f.write("0")
        elif i == 0 or i == 1 or j == 0 or j == 1:
            f.write("1")
        else:
            f.write("0")

        if j != n - 1:
            f.write(" ")
        else:
            f.write("\n")

f.close()
