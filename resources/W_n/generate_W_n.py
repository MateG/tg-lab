import sys

if len(sys.argv) != 2:
    print("Expected a single argument")
    exit(1)

n = int(sys.argv[1])
filename = "W_{}.txt".format(sys.argv[1])

print("Generating {}".format(filename))
f = open(filename, "w")

f.write("{}\n\n".format(n))

for i in range(n):
    left = (i - 1) % (n - 1)
    right = (i + 1) % (n - 1)
    for j in range(n):
        if i == j:
            f.write("0")
        elif i == n - 1 or j == n - 1:
            f.write("1")
        elif j == left or j == right:
            f.write("1")
        else:
            f.write("0")

        if j != n - 1:
            f.write(" ")
        else:
            f.write("\n")

f.close()
