use std::collections::HashSet;
use std::env;
use std::fs;
use std::io::{self, Read};
use std::process;

type MatrixElement = u16;
type Matrix = Vec<Vec<MatrixElement>>;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Expected a single argument - path to the matrix file");
        process::exit(1);
    }

    let read_result = file_contents(&args[1]);
    if read_result.is_err() {
        println!("Could not find or read {}", &args[1]);
        process::exit(1);
    }

    let contents = read_result.unwrap();
    let matrix = parse_matrix(&contents);

    println!("{}", count_triangles(&matrix));
}

fn file_contents(s: &str) -> io::Result<String> {
    let mut file = fs::File::open(s)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(s)
}

fn parse_matrix<'a>(contents: &'a str) -> Matrix {
    let mut lines = contents.lines();
    let first_line = match lines.next() {
        Some(line) => line,
        None => {
            println!("Expected at least 3 lines");
            process::exit(1);
        }
    };

    let n = match first_line.parse::<usize>() {
        Ok(value) => value,
        Err(..) => {
            println!("Unable to parse first line");
            process::exit(1);
        },
    };

    // Consume empty second line.
    lines.next();

    let matrix: Matrix = lines.take(n).map(|line| {
        line.split_whitespace().map(|word| {
            match word.parse() {
                Ok(value) => value,
                Err(..) => {
                    println!("Unable to parse {}", word);
                    process::exit(1);
                },
            }
        }).collect()
    }).collect();

    if matrix.len() != n {
        println!("Matrix should be NxN");
        process::exit(1);
    }

    return matrix;
}

fn count_triangles(matrix: &Matrix) -> usize {
    let mut count = 0;
    let mut visited = HashSet::new();

    let n = matrix.len();
    for row in 0..n {
        for col in row + 1..n {
            if matrix[row][col] == 1 {
                // Edge between vertices at row and col.
                for i in 0..n {
                    if matrix[row][i] == 1 && matrix[col][i] == 1 {
                        let mut triangle = [row, col, i];
                        triangle.sort();
                        if !visited.contains(&triangle) {
                            visited.insert(triangle);
                            count += 1;
                        }
                    }
                }
            }
        }
    }

    return count;
}
