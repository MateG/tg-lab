use std::env;
use std::fs;
use std::io::{self, Read};
use std::process;

type MatrixElement = u16;
type Matrix = Vec<Vec<MatrixElement>>;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Expected a single argument - path to the matrix file");
        process::exit(1);
    }

    let read_result = file_contents(&args[1]);
    if read_result.is_err() {
        println!("Could not find or read {}", &args[1]);
        process::exit(1);
    }

    let contents = read_result.unwrap();
    let matrix = parse_matrix(&contents);

    println!("{}", chromatic_number(&matrix));
}

fn file_contents(s: &str) -> io::Result<String> {
    let mut file = fs::File::open(s)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(s)
}

fn parse_matrix<'a>(contents: &'a str) -> Matrix {
    let mut lines = contents.lines();
    let first_line = match lines.next() {
        Some(line) => line,
        None => {
            println!("Expected at least 3 lines");
            process::exit(1);
        }
    };

    let n = match first_line.parse::<usize>() {
        Ok(value) => value,
        Err(..) => {
            println!("Unable to parse first line");
            process::exit(1);
        },
    };

    // Consume empty second line.
    lines.next();

    let matrix: Matrix = lines.take(n).map(|line| {
        line.split_whitespace().map(|word| {
            match word.parse() {
                Ok(value) => value,
                Err(..) => {
                    println!("Unable to parse {}", word);
                    process::exit(1);
                },
            }
        }).collect()
    }).collect();

    if matrix.len() != n {
        println!("Matrix should be NxN");
        process::exit(1);
    }

    return matrix;
}

fn chromatic_number(matrix: &Matrix) -> usize {
    if is_null_graph(matrix) {
        return 1;
    } else if is_fully_connected_graph(matrix) {
        return matrix.len();
    }

    let all_possible: Vec<usize> = (2..matrix.len() + 1).collect();
    let mut slice = &all_possible[..];
    while slice.len() > 1 {
        let index = slice.len() / 2 - 1;
        let k = slice[index];

        if is_k_colored(matrix, k) {
            slice = &slice[..=index];
        } else {
            slice = &slice[index + 1..];
        }
    }

    return slice[0];
}

fn is_null_graph(matrix: &Matrix) -> bool {
    let n = matrix.len();
    for row in 0..n {
        for col in row + 1..n {
            if matrix[row][col] != 0 {
                return false;
            }
        }
    }

    return true;
}

fn is_fully_connected_graph(matrix: &Matrix) -> bool {
    let n = matrix.len();
    for row in 0..n {
        for col in row + 1..n {
            if matrix[row][col] == 0 {
                return false;
            }
        }
    }

    return true;
}

fn is_k_colored(matrix: &Matrix, k: usize) -> bool {
    let n = matrix.len();
    let mut colored_vertices = ModuloVector::new(n, k);

    let times = usize::pow(k, n as u32);
    for _ in 0..times {
        let mut valid = true;
        'rows_loop: for i in 0..n {
            for j in i + 1..n {
                if matrix[i][j] != 0 {
                    // There's an edge.
                    if colored_vertices.values[i] == colored_vertices.values[j] {
                        // Its vertices are colored with the same color.
                        valid = false;
                        break 'rows_loop;
                    }
                }
            }
        }

        if valid {
            return true;
        }

        loop {
            colored_vertices.increment();

            if colored_vertices.has_k_colors(k) {
                break;
            }
        }
    }

    return false;
}

struct ModuloVector {
    k: usize,
    values: Vec<usize>,
}

impl ModuloVector {
    pub fn new(n: usize, k: usize) -> Self {
        Self {
            k,
            values: vec![0; n],
        }
    }

    pub fn increment(&mut self) {
        for value in self.values.iter_mut().rev() {
            *value += 1;
            if *value >= self.k {
                *value = 0;
            } else {
                break;
            }
        }
    }

    pub fn has_k_colors(&self, k: usize) -> bool {
        let mut color_flags = vec![false; k];
        for value in self.values.iter() {
            color_flags[*value] = true;
        }

        return color_flags.iter().all(|&flag| flag);
    }
}
